import React, {Component} from 'react';

class Numbers extends Component {
    state = {
        numbers : []
    };

    NewNumbersClick = ()=>{
      const numbers = [...this.state.numbers];
      for (let i = 0; i<5; i++){
          const RandomNumbers = 5 + Math.floor(Math.random()*(36-5));
          if (numbers.includes(RandomNumbers)){
              console.log("Game start");
          }else {
              numbers[i] = RandomNumbers;
          }
          numbers.sort(function (a, b) {
              return a-b;
          });
      }
      this.setState({numbers})
    };


    render() {
        return (
            <div className="content">
                <button onClick={this.NewNumbersClick.bind(this)}>New numbers</button>
                <div className="numbers">
                    <p>{this.state.numbers[0]}</p>
                    <p>{this.state.numbers[1]}</p>
                    <p>{this.state.numbers[2]}</p>
                    <p>{this.state.numbers[3]}</p>
                    <p>{this.state.numbers[4]}</p>
                </div>
            </div>
        );
    }
}

export default Numbers;